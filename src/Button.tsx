import React from 'react';

export function Button(props: {onClick: ()=>void, children: React.ReactChild}){
    return <a className="myButton" onClick={props.onClick}>{props.children}</a>
}
