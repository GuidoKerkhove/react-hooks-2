import React from 'react';
import { Button } from './Button';
import { Post } from './Post';


export function Home() {
    const posts: any = null;

    if(!posts){
        return <span>Loading...</span>
    }

    return (
        <>
            <div style={{ margin: '15px 15px 0 15px' }}>
                <form onSubmit={(e) => { e.preventDefault();}}>
                    <input value={""} onChange={(e) => console.log('changed')}></input>
                    <div>
                        <Button onClick={()=>{}}>Zoeken!</Button>
                        <Button onClick={()=>{}}>Legen!</Button>
                    </div>
                </form>
            </div>
            <div>
                {Object.values(posts).map((post: any) =>
                    <Post key={post.id} post={post} subreddit={''}/>
                )}
            </div>
        </>
    )
}
