import axios from 'axios';

function getSubRedditUrl(subreddit: string) {
    return `https://www.reddit.com/r/${subreddit}.json`
}

function getPostDetailUrl(subreddit: string, id: string) {
    return `https://www.reddit.com/r/${subreddit}/comments/${id}.json`;
}

export async function getSubReddit(subreddit: string): Promise<{} | IRedditPostObjectType> {
    if (subreddit === '') {
        return {};
    }
    const url = getSubRedditUrl(subreddit);
    try {
        const resp = await axios.get(url);
        return parseData(resp.data);
    } catch (e) {
        return {};
    }
}

export async function getPostDetails(subreddit: string, id: string): Promise<IPostDetails | undefined> {
    if (subreddit === '' || id === '') {
        return;
    }
    const url = getPostDetailUrl(subreddit, id);
    try {
        const resp = await axios.get(url);
        return parsePostData(resp);
    } catch (e) {
        return;
    }
}

function parseData(data: any) {
    const posts = data.data.children.map((child: any) => ({ ...child.data, hidden: true, selftext_html: decodeHtml(child.data.selftext_html) }))
    const postsObject: IRedditPostObjectType = {};
    posts.forEach((post: IRedditPost) => {
        postsObject[post.id] = post;
    })

    return postsObject
}

function parsePostData(data: any) {
    if (data.data.length !== 2) {
        return;
    }
    const post = Object.values(parseData(data.data[0]))[0];
    const comments = data.data[1].data.children.map((child: any) => {
        return {
            ...child.data,
            body_html: decodeHtml(child.data.body_html),
            replies: child.data.replies ? parseReplies(child.data.replies.data) : undefined
        }
    });
    const commentsObject: IRedditCommentObjectType = {};
    comments.forEach((comment: IRedditComment) => {
        commentsObject[comment.id] = comment;
    });

    function parseReplies(replies: any) {
        const replyArr = replies.children.map((child: any) => {
            return {
                ...child.data,
                body_html: decodeHtml(child.data.body_html),
                replies: child.data.replies ? parseReplies(child.data.replies.data) : undefined
            }
        });
        const replyObject: IRedditCommentObjectType = {};
        replyArr.forEach((reply: IRedditComment) => {
            replyObject[reply.id] = reply;
        });
        return replyObject;
    }

    return {
        post,
        comments: commentsObject
    }

}

function decodeHtml(str: string) {
    var elem = document.createElement('textarea');
    elem.innerHTML = str;
    var decoded = elem.value;
    return decoded
}

export interface IPostDetails {
    post: IRedditPost,
    comments: IRedditCommentObjectType
}

export type IRedditPostObjectType = { [key: string]: IRedditPost }
export type IRedditCommentObjectType = { [key: string]: IRedditComment }

export interface IRedditComment {
    body_html: string;
    author: string;
    id: string;
    replies?: IRedditCommentObjectType;
}

export interface IRedditPost {
    id: string;
    author: string;
    domain: string;
    selftext: string;
    selftext_html: string;
    title: string;
    url: string;
    score: string;
    hidden: boolean;
    secure_media?: {
        oembed?: {
            thumbnail_url: string;
        }
    }
}
