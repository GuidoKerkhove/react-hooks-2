import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Home } from './Home';
import { PostDetail } from './PostDetail';


export function App() {

  return (
    <Router>
      <div className="top-level" style={{ padding: '5px 0 0 10px', borderBottom: '2px solid black', position: 'fixed', height: '30px', width: '100%', backgroundColor: '#FFFFFF' }}>
        <Link to={'/'}>Home</Link>
      </div>
      <div style={{ paddingTop: '30px' }}>
        <Route path="/post/:subreddit/:id" exact component={PostDetail} />
        <Route path="/" exact component={Home} />
      </div>
    </Router>
  );
}
