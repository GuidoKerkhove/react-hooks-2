import React from 'react';
import { IRedditComment } from './util/redditApi';


export function Comment(props: { comment: IRedditComment }) {
    const { comment } = props;
    return (
        <div style={{ border: '1px solid #cccccc', borderRadius: '15px', margin: '10px 10px 10px 10px', padding: '10px 0 0 10px' }}>
            <span style={{ fontWeight: 'bold' }}>Author: {comment.author}</span>
            {comment.body_html !== 'undefined' && <div dangerouslySetInnerHTML={{ __html: comment.body_html }} />}
            {comment.body_html === 'undefined' && <div>Deleted</div>}
            {comment.replies && Object.values(comment.replies).map(reply =>
                <Comment comment={reply} key={reply.id} />
            )}
        </div>
    )
}
