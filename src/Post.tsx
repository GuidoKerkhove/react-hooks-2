import React from 'react';
import { IRedditPost } from './util/redditApi';
import { Link } from 'react-router-dom';

export function Post(props: { post: IRedditPost, subreddit: string }) {
    return (
        <div style={{ border: '1px solid black', margin: '15px 15px 0 15px' }}>
            <span style={{ padding: '10px 10px 30px 10px', display: 'block', fontWeight: 'bold' }}>
                <Link to={`/post/${props.subreddit}/${props.post.id}`} >{props.post.title}</Link>
                <span onClick={() => { }} style={{ float: 'right', fontSize: '36px', cursor: 'pointer' }}>{props.post.hidden ? '+' : '-'}</span>
            </span>
            {!props.post.hidden &&
                <>
                    {props.post.secure_media && props.post.secure_media.oembed
                        ? <img style={{ padding: '0 10px 10px 10px' }} src={props.post.secure_media.oembed.thumbnail_url} />
                        : <div style={{ padding: '0 10px 10px 10px' }} dangerouslySetInnerHTML={{ __html: props.post.selftext_html }} />
                    }
                </>
            }
        </div>
    )
}
