import React from 'react';
import { Comment } from './Comment';

interface IPostDetail {
    match: {
        params: {
            id: string;
            subreddit: string;
        }
    }
}

export function PostDetail(props: IPostDetail) {
    const data: any = null;
    const commentsLength = Object.keys(data.comments).length;

    if (!data) {
        return <span>Loading post....</span>
    }
    return (
        <div style={{ margin: '15px 15px 15px 15px' }}>
            <div>
                <h3 style={{ display: 'inline-block' }}>{data.post.title}</h3> by <h3 style={{ fontWeight: 'bold', display: 'inline-block' }}>{data.post.author}</h3>
            </div>
            <div dangerouslySetInnerHTML={{ __html: data.post.selftext_html }} />
            <div style={{ border: '1px solid #666666', padding: '0 0 0 10px' }}>Comments: {commentsLength}</div>
            {commentsLength > 0 ?
                <div style={{ maxHeight: '800px', overflowY: 'scroll' }}>
                    {Object.values(data.comments).map((comment: any) => (
                        <Comment key={comment.id} comment={comment} />
                    ))}
                </div>
                : <div style={{ padding: '0 0 0 10px' }}>Geen comments</div>
            }
        </div>
    )
}
